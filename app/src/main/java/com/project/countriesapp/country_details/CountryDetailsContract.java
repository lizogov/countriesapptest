
package com.project.countriesapp.country_details;


import android.content.Context;

import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;


public interface CountryDetailsContract {

    interface Model {

        interface OnFinishedListener {
            void onFinished( CountryModel countryModel,ArrayList<CountryModel> borders);

            void onFailure(String t);
        }

        void getCountryDetailsCode(Context context, OnFinishedListener onFinishedListener, String countryCode);

    }

    interface View {

        void setDataToViews(CountryModel countryModel,ArrayList<CountryModel> borders);

        void onResponseFailure(String throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestCountryData(Context context, String countryCode);
    }
}
