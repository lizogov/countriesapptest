package com.project.countriesapp.country_details;


import android.content.Context;
import android.util.Log;

import com.project.countriesapp.db.AppDatabase;
import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CountryDetailsModel implements CountryDetailsContract.Model {

    @Override
    public void getCountryDetailsCode(Context context, final OnFinishedListener onFinishedListener, String countryCode) {
        CountryModel res = AppDatabase.getInstance(context).countriesDao().getByCode(countryCode);
        ArrayList<CountryModel> borders = new ArrayList<>();
        if (res != null) {
            List r = Arrays.asList(res.getBorders().trim().replace(" ","").split(","));
            if (r.size() > 0) {

                for (int i = 0; i < r.size(); i++) {
                    CountryModel bor = AppDatabase.getInstance(context).countriesDao().getByCode1(String.valueOf(r.get(i)));
                    if (bor != null) {
                        borders.add(bor);
                    }
                }

            }

            Log.d("size", "getCountryDetailsCode: " + r.size());
            onFinishedListener.onFinished(res,borders);
        } else {
            onFinishedListener.onFailure("Country not found");
        }
    }

}
