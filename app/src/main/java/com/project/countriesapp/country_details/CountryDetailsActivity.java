package com.project.countriesapp.country_details;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.project.countriesapp.R;
import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;

import static com.project.countriesapp.utils.Constants.COUNTRY_CODE;


public class CountryDetailsActivity extends AppCompatActivity implements CountryDetailsContract.View {


    private TextView detailsCountryName;
    private TextView detailsCountryArea;
    private TextView detailsCountryNativeName;
    private TextView detailsCountryBorders;


    private CountryDetailsPresenter countryDetailsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_details);

        initUI();

        Intent mIntent = getIntent();
        String countryCode = mIntent.getStringExtra(COUNTRY_CODE);

        countryDetailsPresenter = new CountryDetailsPresenter(this);
        countryDetailsPresenter.requestCountryData(getApplicationContext(), countryCode);

    }


    private void initUI() {


        detailsCountryName = findViewById(R.id.details_country_name);
        detailsCountryArea = findViewById(R.id.details_country_area);
        detailsCountryNativeName = findViewById(R.id.details_country_NativeName);
        detailsCountryBorders = findViewById(R.id.details_country_borders);

    }


    @Override
    public void setDataToViews(CountryModel countryModel, ArrayList<CountryModel> borders) {

        if (countryModel != null) {

            detailsCountryName.setText(countryModel.getName());

            detailsCountryArea.setText(String.valueOf(countryModel.getArea()));

            detailsCountryNativeName.setText(countryModel.getNativeName());

            if (borders.size() > 0) {
                StringBuilder bordersTxt = new StringBuilder();
                for (int i = 0; i < borders.size(); i++) {
                    bordersTxt.append(borders.get(i).getNativeName()).append(" - ").append(borders.get(i).getName()).append("\n");
                }
                detailsCountryBorders.setText(bordersTxt.toString());
            } else
                detailsCountryBorders.setText("");


//            countryDetailsPresenter.requestCountryData(getApplicationContext(), String.valueOf(r.get(0)));
//            String genreRes = "";
//            for (int i = 0; i < r.size(); i++) {
//                genreRes = genreRes + r.get(i);
//                if (i < (r.size() - 1)) {
//                    genreRes += ", ";
//                }
//            }
//            Log.d("lll", "setDataToViews: " + genreRes);
//            Picasso.get().load(countryModel.getAlpha3Code()).into(detailsCountryImage);

        }

    }

    @Override
    public void onResponseFailure(String throwable) {

        Toast.makeText(this, throwable, Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        countryDetailsPresenter.onDestroy();
    }


}
