package com.project.countriesapp.country_details;


import android.content.Context;

import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;


public class CountryDetailsPresenter implements CountryDetailsContract.Presenter, CountryDetailsContract.Model.OnFinishedListener {

    private CountryDetailsContract.View countryDetailView;
    private CountryDetailsContract.Model countryDetailsModel;

    CountryDetailsPresenter(CountryDetailsContract.View countryDetailView) {
        this.countryDetailView = countryDetailView;
        this.countryDetailsModel = new CountryDetailsModel();
    }

    @Override
    public void onDestroy() {

        countryDetailView = null;
    }

    @Override
    public void requestCountryData(Context context, String countryCode) {


        countryDetailsModel.getCountryDetailsCode(context, this, countryCode);
    }

    @Override
    public void onFinished(CountryModel countryModel, ArrayList<CountryModel> borders) {

        countryDetailView.setDataToViews(countryModel,borders);
    }

    @Override
    public void onFailure(String t) {

        countryDetailView.onResponseFailure(t);
    }
}
