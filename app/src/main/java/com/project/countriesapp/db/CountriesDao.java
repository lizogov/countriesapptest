package com.project.countriesapp.db;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.project.countriesapp.model.CountryModel;

import java.util.Collection;
import java.util.List;

@Dao
public interface CountriesDao {

    @Query("SELECT * FROM COUNTRIES ORDER BY name ASC")
    List<CountryModel> getAllByNameASC();
    @Query("SELECT * FROM COUNTRIES ORDER BY name DESC")
    List<CountryModel> getAllByNameDESC();

    @Query("SELECT * FROM COUNTRIES ORDER BY area ASC")
    List<CountryModel> getAllByAreaASC();
    @Query("SELECT * FROM COUNTRIES ORDER BY area DESC")
    List<CountryModel> getAllByAreaDESC();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Collection<CountryModel> countryModels);


    @Query("SELECT * FROM COUNTRIES WHERE alpha3Code = :countryId")
    CountryModel getByCode(String countryId);
    @Query("SELECT * FROM COUNTRIES WHERE alpha3Code = :countryId")
    CountryModel getByCode1(String countryId);

    @Query("DELETE FROM COUNTRIES")
    void deleteAll();
}
