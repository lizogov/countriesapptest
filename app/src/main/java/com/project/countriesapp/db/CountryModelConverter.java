package com.project.countriesapp.db;


import com.project.countriesapp.model.Country;
import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;

public class CountryModelConverter {

    public static ArrayList<CountryModel> convertResult(ArrayList<Country> countryListResult) {
        int res = 0;
        ArrayList<CountryModel> result = new ArrayList<>();
        for (Country countryResult : countryListResult) {
            StringBuilder borders = new StringBuilder();
            for (int i = 0; i < countryResult.getBorders().size(); i++) {
                borders.append(countryResult.getBorders().get(i));
                if (i != countryResult.getBorders().size()-1) {
                    borders.append(", ");
                }
            }
//            DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
            Double area;
            if (countryResult.getArea()!=null){
                area = countryResult.getArea();
            }else {
                area = 0.0;
            }


            result.add(new CountryModel(res++, countryResult.getName(), countryResult.getAlpha3Code(),
                    area,
                    borders.toString(),String.valueOf(countryResult.getNativeName())));

        }

        return result;
    }


}
