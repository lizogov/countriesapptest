package com.project.countriesapp.counties_list;

import android.util.Log;

import com.project.countriesapp.db.CountryModelConverter;
import com.project.countriesapp.model.Country;
import com.project.countriesapp.model.CountryModel;
import com.project.countriesapp.network.ApiClient;
import com.project.countriesapp.network.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CountriesListModel implements CountriesListContract.Model {

    private final String TAG = "CountriesListModel";

    @Override
    public void getCountriesList(final OnFinishedListener onFinishedListener) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Country>> call = apiService.getCountries();
        call.enqueue(new Callback<ArrayList<Country>>() {
            @Override
            public void onResponse(Call<ArrayList<Country>> call, Response<ArrayList<Country>> response) {

                if (response.body() != null) {
                    ArrayList<CountryModel> res = CountryModelConverter.convertResult(response.body());
                    Log.d(TAG, "Number of countries received: " + res.size());
                    onFinishedListener.onFinished(res);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Country>> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                onFinishedListener.onFailure(t);
            }
        });
    }
}
