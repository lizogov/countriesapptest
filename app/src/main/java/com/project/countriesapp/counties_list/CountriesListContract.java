

package com.project.countriesapp.counties_list;


import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;

public interface CountriesListContract {

    interface Model {

        interface OnFinishedListener {
            void onFinished(ArrayList<CountryModel> countryModels);

            void onFailure(Throwable t);
        }

        void getCountriesList(OnFinishedListener onFinishedListener);

    }

    interface View {

        void showProgress();

        void hideProgress();

        void sortByNameAsc(ArrayList<CountryModel> countryModels);
        void sortByNameDesc(ArrayList<CountryModel> countryModels);
        void sortByAreaAsc(ArrayList<CountryModel> countryModels);
        void sortByAreaDesc(ArrayList<CountryModel> countryModels);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void onDestroy();

        void getMoreData();

        void requestDataFromServer();

    }
}
