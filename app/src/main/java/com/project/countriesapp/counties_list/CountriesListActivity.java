package com.project.countriesapp.counties_list;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.project.countriesapp.R;
import com.project.countriesapp.adapter.CountriesAdapter;
import com.project.countriesapp.country_details.CountryDetailsActivity;
import com.project.countriesapp.db.AppDatabase;
import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;

import static com.project.countriesapp.utils.Constants.COUNTRY_CODE;


public class CountriesListActivity extends AppCompatActivity implements CountriesListContract.View, CountryItemClickListener {

    private boolean sortByNameASC = true;
    private boolean sortByAreaASC = false;

    private static final String TAG = "CountriesListActivity";
    private CountriesListPresenter countriesListPresenter;
    private RecyclerView rv;
    private ArrayList<CountryModel> countriesList;
    private CountriesAdapter countriesAdapter;
    private LottieAnimationView lottieAnimationView;
    private TextView tvEmptyView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_list);

        initUI();

        countriesListPresenter = new CountriesListPresenter(this);
        countriesListPresenter.getMoreData();
    }

    private void initUI() {

        rv = findViewById(R.id.recyclerview_list);
        countriesList = new ArrayList<>();
        countriesAdapter = new CountriesAdapter(this, countriesList);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(countriesAdapter);
        lottieAnimationView = findViewById(R.id.anim_loading);
        tvEmptyView = findViewById(R.id.tv_empty_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sorting_item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.sort_by_name :
                if (sortByNameASC){
                    countriesList.clear();
                    countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByNameDESC());
                    countriesAdapter.notifyDataSetChanged();
                    sortByNameASC = false;
                    item.setTitle(R.string.by_name_a_z);
                }else{
                    countriesList.clear();
                    countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByNameASC());
                    countriesAdapter.notifyDataSetChanged();
                    sortByNameASC = true;
                    item.setTitle(R.string.by_name_z_a);
                }

                break;
            case R.id.sort_by_area :
                if (!sortByAreaASC){
                    countriesList.clear();
                    countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByAreaASC());
                    countriesAdapter.notifyDataSetChanged();
                    sortByAreaASC = true;
                    item.setTitle(R.string.by_area_9_1);

                }else{
                    countriesList.clear();
                    countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByAreaDESC());
                    countriesAdapter.notifyDataSetChanged();
                    sortByAreaASC = false;
                    item.setTitle(R.string.by_area_1_9);
                }
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {

        lottieAnimationView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {

        lottieAnimationView.setVisibility(View.GONE);

    }

    @Override
    public void sortByNameAsc(ArrayList<CountryModel> countryModels) {

        AppDatabase.getInstance(CountriesListActivity.this).countriesDao().deleteAll();
        AppDatabase.getInstance(CountriesListActivity.this).countriesDao().insertAll(countryModels);
        countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByNameASC());
        countriesAdapter.notifyDataSetChanged();
        sortByNameASC = true;
    }

    @Override
    public void sortByNameDesc(ArrayList<CountryModel> countryModels) {
        countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByNameDESC());
        countriesAdapter.notifyDataSetChanged();
        sortByNameASC = false;
    }

    @Override
    public void sortByAreaAsc(ArrayList<CountryModel> countryModels) {
        countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByAreaASC());
        countriesAdapter.notifyDataSetChanged();
        sortByAreaASC = true;
    }

    @Override
    public void sortByAreaDesc(ArrayList<CountryModel> countryModels) {
        countriesList.addAll(AppDatabase.getInstance(CountriesListActivity.this).countriesDao().getAllByAreaDESC());
        countriesAdapter.notifyDataSetChanged();
        sortByAreaASC = false;
    }


    @Override
    public void onResponseFailure(Throwable throwable) {


        Log.e(TAG, throwable.getMessage());
        Toast.makeText(this, getString(R.string.communication_error), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countriesListPresenter.onDestroy();
    }

    @Override
    public void onCountryItemClick(int position) {

        if (position == -1) {
            return;
        }
        Intent detailIntent = new Intent(this, CountryDetailsActivity.class);
//        Toast.makeText(this, countriesList.get(position).getId(), Toast.LENGTH_SHORT).show();
        detailIntent.putExtra(COUNTRY_CODE, countriesList.get(position).getAlpha3Code());
        startActivity(detailIntent);
    }


}
