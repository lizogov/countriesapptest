
package com.project.countriesapp.counties_list;

public interface CountryItemClickListener {

    void onCountryItemClick(int position);
}
