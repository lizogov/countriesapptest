
package com.project.countriesapp.counties_list;


import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;

public class CountriesListPresenter implements CountriesListContract.Presenter, CountriesListContract.Model.OnFinishedListener {

    private CountriesListContract.View countriesListView;

    private CountriesListContract.Model countriesListModel;

    CountriesListPresenter(CountriesListContract.View countriesListView) {
        this.countriesListView = countriesListView;
        countriesListModel = new CountriesListModel();
    }

    @Override
    public void onDestroy() {
        this.countriesListView = null;
    }

    @Override
    public void getMoreData() {

        if (countriesListView != null) {
            countriesListView.showProgress();
        }
        countriesListModel.getCountriesList(this);
    }

    @Override
    public void requestDataFromServer() {

        if (countriesListView != null) {
            countriesListView.showProgress();
        }
        countriesListModel.getCountriesList(this);
    }

    @Override
    public void onFinished(ArrayList<CountryModel> countryModels) {
        countriesListView.sortByNameAsc(countryModels);
        if (countriesListView != null) {
            countriesListView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {

        countriesListView.onResponseFailure(t);
        if (countriesListView != null) {
            countriesListView.hideProgress();
        }
    }

    public void getAllByNameAsc() {
        countriesListModel.getCountriesList(this);
    }

    public void getAllByNameDesc() {
        countriesListModel.getCountriesList(this);
    }
}
