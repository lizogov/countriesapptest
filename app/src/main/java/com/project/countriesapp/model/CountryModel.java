package com.project.countriesapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "COUNTRIES")
public class CountryModel implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String alpha3Code;
    private Double area;
    private String borders;
    private String nativeName;





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }
//    private String trailerUrl;

    public CountryModel() {
    }

    protected CountryModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        alpha3Code = in.readString();
        area = in.readDouble();
        borders = in.readString();
        nativeName = in.readString();

    }

    public CountryModel(int id, String name, String alpha3Code, Double area, String borders, String nativeName) {
        this.id = id;
        this.name = name;
        this.alpha3Code = alpha3Code;
        this.area = area;
        this.borders = borders;
        this.nativeName = nativeName;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageRes() {
        return alpha3Code;
    }


    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public String getAlpha3Code() {
        return alpha3Code;
    }


    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    public String getBorders() {
        return borders;
    }

    public void setBorders(String borders) {
        this.borders = borders;
    }



    @Override
    public String toString() {
        return "CountryModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", alpha3Code='" + alpha3Code + '\'' +
                ", area='" + area + '\'' +
                ", borders='" + borders + '\'' +
                ", nativeName='" + nativeName + '\'' +
                '}';
    }

    public static final Creator<CountryModel> CREATOR = new Creator<CountryModel>() {
        @Override
        public CountryModel createFromParcel(Parcel in) {
            return new CountryModel(in);
        }

        @Override
        public CountryModel[] newArray(int size) {
            return new CountryModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(String.valueOf(id));
        parcel.writeString(name);
        parcel.writeString(alpha3Code);
        parcel.writeDouble(area);
        parcel.writeString(borders);
        parcel.writeString(nativeName);

    }
}
