package com.project.countriesapp.network;


import com.project.countriesapp.model.Country;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiInterface {

    @GET("rest/v2/all")
    Call<ArrayList<Country>> getCountries();

}
