
package com.project.countriesapp.adapter;

import android.graphics.drawable.PictureDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestBuilder;
import com.project.countriesapp.R;
import com.project.countriesapp.counties_list.CountriesListActivity;
import com.project.countriesapp.model.CountryModel;

import java.util.ArrayList;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.MyViewHolder> {



    private CountriesListActivity countriesListActivity;
    private ArrayList<CountryModel> countriesList;
    private RequestBuilder<PictureDrawable> requestBuilder;

    public CountriesAdapter(CountriesListActivity countriesListActivity, ArrayList<CountryModel> countriesList) {
        this.countriesListActivity = countriesListActivity;
        this.countriesList = countriesList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        CountryModel countryModel = countriesList.get(position);

        holder.countryNativeName.setText(countryModel.getNativeName());
        holder.countryName.setText(countryModel.getName());
        holder.itemView.setOnClickListener(view -> countriesListActivity.onCountryItemClick(position));

    }

    @Override
    public int getItemCount() {
        return countriesList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView countryNativeName;
        TextView countryName;

        MyViewHolder(View itemView) {
            super(itemView);
            countryNativeName = itemView.findViewById(R.id.item_country_nativeName);
            countryName = itemView.findViewById(R.id.item_country_name);

        }
    }
}
